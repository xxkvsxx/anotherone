<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "../css/menu.css";
        @import "../css/home.css";
        #mine
        {
            border-color: rgb(255, 255, 0);
        }
        .comm
        {
            color: rgb(250, 100, 0);
            background-color: black;
            border-radius: 10%;
            border-width: 3px;
            border-style: solid;
            border-color: rgb(250, 100, 0);
            padding: 2%;
        }
        .comm > h5
        {
            margin: 2%;
            font-size: 250%;
            font-family: 'Airborne';
        }
        .comm > p
        {
            font-size: 600%;
            font-family: 'HelveticaRegular';
            color: rgb(255, 140, 0);
        }
        .newcomm
        {
            color: rgb(250, 100, 0);
            background-color: black;
            text-align: center;
        }
        .newcomm > a
        {
            background-color: black;
            color: rgb(255, 140, 0);
            font-size: 500%;
            font-family: 'Airborne';
        }
        hr
        {
            margin: 0%;
        }
    </style>
    <link rel="shortcut icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="../index.js"></script>
</head>
<body onresize="res(false)">
    <script>res(false)</script>
    <div class="cell1"></div>
    <div class="cell10 main">
        <h1 class="h1">Комментарии</h1>
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>

    <div class="cell1"></div>
    <div class="cell10">
        <?php
            require("../php/script.php");
            upPointer();
            $num = $_GET["photonum"];
            $off = $_GET["off"];
            $id = $_COOKIE["uniqueid"];
            $mysqli = new mysqli("mysql.hostinger.ru", "u916626188_kvs", "qaz123", "u916626188_one");
            if($mysqli->connect_errno)
            {
                echo("Серверная ошибка, приношу свои извинения...");
                exit;
            }
            else
            {
                $S = "<div class=\"cell12 newcomm\"><a href=\"newcomm.html?photonum=";
                $S = $S.((string)$num)."\">Добавить Комментарий</a></div>";
                echo($S);
                $r1 = $mysqli->query("SELECT * FROM Comments WHERE NUM = \"".$num."\" ORDER BY DT DESC");
                $S = "";
                while($r = $r1->fetch_assoc())
                {
                    if($id == $r["ID"]) $S = "<div id=\"mine\" class=\"cell12 comm\">";
                    else $S = "<div class=\"cell12 comm\">";
                    $S = $S."<h5>ID:&nbsp;".$r["ID"];
                    $dt = DateTime::createFromFormat("Y-m-d H:i:s", $r['DT'])->getTimestamp();
                    $dt += ((int)$_GET["off"])*60;
                    $S = $S."<br>DATE:&nbsp;".date("d.m.Y H:i:s", $dt)."</h5><hr>";
                    $S = $S."<p> ".$r["TEXT"]."</p>";
                    $S = $S."</div>";
                    echo($S);
                }
            }
        ?>
    </div>
    <div class="cell1"></div>
</body>
</html>