var GK = 16;
var GP = [
    { filename: "abstract.png", resolution: "1920x1200 px", filesize: "2.2 МБ" },
    { filename: "1.png", resolution: "2560x1600 px", filesize: "1.4 МБ" },
    { filename: "2.png", resolution: "1920x1080 px", filesize: "1.6 МБ" },
    { filename: "3D.jpg", resolution: "640x960 px", filesize: "179 КиБ" },
    { filename: "Абхазия.jpg", resolution: "4160x3120 px", filesize: "7.4 МБ" },
    { filename: "Небольшая Радуга.jpg", resolution: "4160x2340 px", filesize: "2.8 МБ" },
    { filename: "Скорпион - Абхазия.jpg", resolution: "4160x3120 px", filesize: "3.3 МБ" },
    { filename: "ГЭС - Новый Афон.jpg", resolution: "4160x3120 px", filesize: "4.2 МБ" },
    { filename: "Night.jpg", resolution: "1680x1050 px", filesize: "410 КиБ" },
    { filename: "9.png", resolution: "443x443 px", filesize: "400 КиБ" },
    { filename: "10.png", resolution: "1030x1030 px", filesize: "2.6 МБ" },
    { filename: "11.png", resolution: "1200x1200 px", filesize: "2.6 МБ" },
    { filename: "12.jpg", resolution: "4600x3067 px", filesize: "2.4 МБ" },
    { filename: "Pikachu.png", resolution: "1680x1050 px", filesize: "159 КиБ" },
    { filename: "Rainbow.jpg", resolution: "240x320 px", filesize: "15.3 КиБ" },
    { filename: "Winter.jpg", resolution: "1920x1080 px", filesize: "600 КиБ" }
]
var KC = getCookie("photonum");
var GD = new Image();
if((!window.location.hash) && (KC))
{
    GD.src = "./data/"+GP[KC];
    window.location.hash = "photonum="+parseInt(KC);
}
/* Загрузка миниатюр */
function loadmin()
{
    var galery = getClassElements(document, "galery")[0];
    for(var i=0; i<GK; ++i)
    {
        var dv = document.createElement("div");
        dv.className = "mcell6 cell3 mini";
        dv.id = "mini"+i;
        dv.setAttribute("onclick", "addLocation("+i+")");
        var im = document.createElement("img");
        im.setAttribute("src", "./min/"+i+".png");
        im.setAttribute("alt", GP[i].filename);
        im.setAttribute("title", GP[i].filename);
        im.setAttribute("onload", "photores()");
        dv.appendChild(im);
        galery.appendChild(dv);
    }
    hashchange();
}

var PREV = false;
var PR = new Image();
PR.src = "../css/images/preview.png";

var LOAD = new Image();
LOAD.src = "../css/images/sync.gif";

var imnext = new Image();
imnext.obj = null;
imnext.src = "../css/images/sync.gif";
imnext.nxsrc = "../css/images/sync.gif";
imnext.onload = function () 
{
    if(imnext.obj)
    {
        var BL2 = document.getElementById("BLOCK2");
        if(!BL2)
        {
            if(mq)
            {
                var H = PREVBlocksP[2].height.height;
                var W = PREVBlocksP[2].width.width;
            }
            else
            {
                var H = PREVBlocks[2].height.height;
                var W = PREVBlocks[2].width.width;
            }
        }
        else
        {
            var H = parseInt(BL2.style.height, 10);
            var W = parseInt(BL2.style.width, 10);
        }
        var A = ieRES(this.width, this.height, Math.ceil(W*95/100), Math.ceil(H*95/100));
        imnext.obj.per = 95;
        imnext.obj.style.width = A.x+"px";
        imnext.obj.style.height = A.y+"px";
        imnext.obj.src = this.src;
        imnext.obj.style.borderWidth = "2px";
        imauto.src = imnext.nxsrc;
    } 
};

var imauto = new Image();
imauto.fl = false;
imauto.src = "./data/"+GP[0].filename;
imauto.onload = function () { if(this.src != "") this.fl = true; }
imauto.checksrc = function (sr, im)
{
    if((this.fl) && (this.src == sr)) im.src = this.src;
    else
    {
        var BL2 = document.getElementById("BLOCK2");
        if(!BL2)
        {
            if(mq)
            {
                var H = PREVBlocksP[2].height.height;
                var W = PREVBlocksP[2].width.width;
            }
            else
            {
                var H = PREVBlocks[2].height.height;
                var W = PREVBlocks[2].width.width;
            }
        }
        else
        {
            var H = parseInt(BL2.style.height, 10);
            var W = parseInt(BL2.style.width, 10);
        }
        var A = ieRES(LOAD.width, LOAD.height, Math.ceil(W*30/100), Math.ceil(H*30/100));
        im.per = 30;
        im.style.width = A.x+"px";
        im.style.height = A.y+"px";
        im.style.borderWidth = "0px";
        im.src = LOAD.src;
        imnext.obj = im;
        imnext.src = sr;
        this.src = "";
        this.fl = false;
    }
}


function photores()
{
    var bl = getClassElements(document, "mini");
    var H = 256;
    for(var i=0; i<bl.length; ++i)
    {
        bl[i].style.height = H + "px";
        bl[i].style.lineHeight = H + "px";
    }
    var m = 4;
    if(mq) m = 2;
    var i=0;
    while(i<bl.length)
    {
        var P = 0;
        for(var j=i; j<i+m; ++j)
        {
            var im = bl[j].getElementsByTagName("img")[0];
            if(im.height > P) P = im.height;
        }
        H = Math.min(256, P);
        for(var j=i; j<i+m; ++j)
        {
            bl[j].style.height = H + "px";
            bl[j].style.lineHeight = H + "px";
        }
        i += m;
    }
    if (PREV) PREVres();
}

function addLocation(n)
{
    HASH = window.location.hash;
    window.location.hash = "#photonum="+n;
}

var PREVBlocks = [
    { height: { top: 50, height: 855, lineHeight: 855 }, width: { left: 0, width: 150 }, border: true },
    { height: { top: 50, height: 855, lineHeight: 855 }, width: { left: 1150, width: 150 }, border: true },
    { height: { top: 50, height: 855, lineHeight: 855 }, width: { left: 150, width: 1000 }, border: true },
    { height: { top: 90, height: 300 }, width: { left: 1330, width: 560 }, border: true },
    { height: { top: 10, height: 100, lineHeight: 100 }, width: { right: 10, width: 100 }, border: false },
    { height: { top: 400, height: 50, lineHeight: 50 }, width: { left: 1330, width: 560 }, border: true },
    { height: { top: 450, height: 50, lineHeight: 50 }, width: { left: 1330, width: 560 }, border: true },
    { height: { top: 500, height: 400 }, width: { left: 1330, width: 560 }, border: true }
]

var PREVBlocksP = [
    { height: { top: 490, height: 100, lineHeight: 100 }, width: { left: 20, width: 900 }, border: true },
    { height: { top: 490, height: 100, lineHeight: 100 }, width: { left: 980, width: 900 }, border: true },
    { height: { top: 75, height: 400, lineHeight: 400 }, width: { left: 20, width: 1880 }, border: true },
    { height: { top: 600, height: 150 }, width: { left: 20, width: 900 }, border: true },
    { height: { top: 5, height: 75, lineHeight: 75 }, width: { right: 0, width: 302 }, border: false },
    { height: { top: 600, height: 75, lineHeight: 75 }, width: { left: 980, width: 900 }, border: true },
    { height: { top: 675, height: 75, lineHeight: 75 }, width: { left: 980, width: 900 }, border: true },
    { height: { top: 760, height: 180 }, width: { left: 20, width: 1880 }, border: true }
]
var PBlocks = PREVBlocks;
var DV7;
function loadPreview()
{
    var bod = document.getElementsByTagName("body")[0];
    bod.style.position = "fixed";
    bod.style.overflow = "hidden";
    var bd = document.createElement("div");
    bd.className = "fx preview";
    var i=0;
    var DV = [];
    while(i < PBlocks.length)
    {
        var dv = document.createElement("div");
        dv.className = "fx";
        dv.id = "BLOCK"+i;
        DV.push(dv);
        bd.appendChild(dv);
        ++i;
    }
    var im = document.createElement("img");
    im.setAttribute("src", "../css/images/left.png");
    im.setAttribute("alt", "left.png");
    im.setAttribute("title", "left.png");
    DV[0].setAttribute("onclick", "goLeft()");
    DV[0].appendChild(im);
    var im = document.createElement("img");
    im.setAttribute("src", "../css/images/right.png");
    im.setAttribute("alt", "right.png");
    im.setAttribute("title", "right.png");
    DV[1].setAttribute("onclick", "goRight()");
    DV[1].appendChild(im);
    var im = document.createElement("img");
    im.setAttribute("src", "../css/images/close.png");
    im.setAttribute("alt", "close.png");
    im.setAttribute("title", "close.png");
    DV[4].setAttribute("onclick", "goClose()");
    DV[4].appendChild(im);
    var im = document.createElement("img");
    var k = parseInt(window.location.hash.split("=")[1]);
    im.id = "PREVIEW";
    im.setAttribute("alt", GP[k].filename);
    im.setAttribute("title", GP[k].filename);
    if(k == (GK-1)) imnext.nxsrc = "./data/"+GP[k-1].filename;
    else imnext.nxsrc = "./data/"+GP[k+1].filename;
    imauto.checksrc("./data/"+GP[k].filename, im);
    DV[2].appendChild(im);
    document.getElementsByTagName("body")[0].appendChild(bd);
    DV7 = DV[3];
    var p = document.createElement("p");
    var t = document.createTextNode("Активировать Default Preview");
    p.appendChild(t);
    DV[5].appendChild(p);
    DV[5].setAttribute("onclick", "activePREVmode();");
    var p = document.createElement("p");
    var t = document.createTextNode("Деактивировать Default Preview");
    p.appendChild(t);
    DV[6].appendChild(p);
    DV[6].setAttribute("onclick", "deactivePREVmode();");
    var ifr = document.createElement("iframe");
    ifr.setAttribute("id", "IFR");
    ifr.setAttribute("src", "comments.php?off="+(-(new Date().getTimezoneOffset()))+"&photonum="+k);
    DV[7].appendChild(ifr);
    PREVres();
    setPREVtext(k);
    imauto.fl = false;
    PREV = true;
}

function PREVres()
{
    if(mq) PBlocks = PREVBlocksP;
    else PBlocks = PREVBlocks;
    for(var i=0; i<PBlocks.length; ++i)
    {
        var dv = document.getElementById("BLOCK"+i);
        var Block = PBlocks[i];
        for(var j in Block.height)
        {
            var H = Math.ceil(height*Block.height[j]/IH);
            if(j.search("eight") != -1) H -= 20;
            dv.style[j] = H + "px";
        }
        for(var j in Block.width)
        {
            var W = Math.ceil(width*Block.width[j]/IW);
            if(j.search("idth") != -1) W -= 20;
            dv.style[j] = W + "px";
        }
    }
    var BL2 = document.getElementById("BLOCK2");
    im = BL2.getElementsByTagName("img")[0];
    temp = new Image();
    temp.src = im.src;
    var H = parseInt(BL2.style.height, 10);
    var W = parseInt(BL2.style.width, 10);
    var A = ieRES(temp.width, temp.height, Math.ceil(W*im.per/100), Math.ceil(H*im.per/100));
    im.style.width = A.x+"px";
    im.style.height = A.y+"px";
    var dv7 = document.getElementById("BLOCK7");
    var ifr = document.getElementById("IFR");
    ifr.style.height = (parseInt(dv7.style.height) - 10)+"px";
    ifr.style.width = (parseInt(dv7.style.width) - 10)+"px";
}

function ieRES(x, y, X, Y)
{
    var A = {};
    if((x <= X) && (y <= Y)) { A.x = x; A.y = y; }
    else if(x <= X) { A.y = Y; A.x = Math.ceil(Y*x/y); }
    else if(y <= Y) { A.x = X; A.y = Math.ceil(X*y/x); }
    else
    {
        A.y = Y;
        A.x = Math.ceil(Y*x/y);
        if(A.x > X) { A.x = X; A.y = Math.ceil(X*y/x); }
    }
    return A;
}

function changePreview()
{
    im = document.getElementById("PREVIEW");
    var k = parseInt(window.location.hash.split("=")[1]);
    if(k == (GK-1)) imnext.nxsrc = "./data/"+GP[k-1].filename;
    else imnext.nxsrc = "./data/"+GP[k+1].filename;
    imauto.checksrc("./data/"+GP[k].filename, im);
    im.setAttribute("alt", GP[k].filename);
    im.setAttribute("title", GP[k].filename);
    ifr = document.getElementById("IFR");
    ifr.setAttribute("src", "comments.php?off="+(-(new Date().getTimezoneOffset()))+"&photonum="+k);
    setPREVtext(k);
}

function setPREVtext(k)
{
    while(DV7.childNodes.length > 0) DV7.removeChild(DV7.childNodes[0]);
    var h1 = document.createElement("h1");
    var t = document.createTextNode("Свойства Изображения");
    h1.appendChild(t);
    DV7.appendChild(h1);
    DV7.appendChild(document.createElement("hr"));
    DV7.appendChild(document.createElement("br"));
    var p = document.createElement("h2");
    var t = document.createTextNode("Имя Файла:  ");
    p.appendChild(t);
    DV7.appendChild(p);
    var p = document.createElement("p");
    var t = document.createTextNode(GP[k].filename);
    p.appendChild(t);
    DV7.appendChild(p);
    DV7.appendChild(document.createElement("br"));
    DV7.appendChild(document.createElement("br"));
    var p = document.createElement("h2");
    var t = document.createTextNode("Разрешение:  ");
    p.appendChild(t);
    DV7.appendChild(p);
    var p = document.createElement("p");
    var t = document.createTextNode(GP[k].resolution);
    p.appendChild(t);
    DV7.appendChild(p);
    DV7.appendChild(document.createElement("br"));
    DV7.appendChild(document.createElement("br"));
    var p = document.createElement("h2");
    var t = document.createTextNode("Размер:  ");
    p.appendChild(t);
    DV7.appendChild(p);
    var p = document.createElement("p");
    var t = document.createTextNode(GP[k].filesize);
    p.appendChild(t);
    DV7.appendChild(p);
    DV7.appendChild(document.createElement("br"));
    DV7.appendChild(document.createElement("br"));
    var p = document.createElement("h2");
    var t = document.createTextNode("Позиция:  ");
    p.appendChild(t);
    DV7.appendChild(p);
    var p = document.createElement("p");
    var t = document.createTextNode((k+1)+"/"+GP.length);
    p.appendChild(t);
    DV7.appendChild(p);
    DV7.appendChild(document.createElement("br"));
}

function goLeft()
{
    var k = parseInt(window.location.hash.split("=")[1]);
    if(k > 0)
    {
        --k;
        HASH = window.location.hash;
        window.location.hash = "#photonum="+k;
    }
}

function goRight()
{
    var k = parseInt(window.location.hash.split("=")[1]);
    if(k < (GK-1))
    {
        ++k;
        HASH = window.location.hash;
        window.location.hash = "#photonum="+k;
    }
}

function closePreview()
{
    if(getClassElements(document, "preview"))
    {
        var A = getClassElements(document, "preview")[0];
        A.parentNode.removeChild(A);
    }
    PREV = false;
}

function goClose()
{
    var bod = document.getElementsByTagName("body")[0];
    bod.style.position = "static";
    bod.style.overflow = "auto";
    HASH = window.location.hash;
    window.location.hash = "mini" + parseInt(HASH.substring(1).split("=")[1]);
}


function keyD(event)
{
    var key = event.which || event.keyCode;
    if(key == 112)
    {
        if(event.preventDefault) event.preventDefault()
        else event.returnValue = false;
    }
}

function keyAction(event)
{
    var key = event.which || event.keyCode;
    if((PREV) && (key == 27)) goClose();
    else if((PREV) && (key == 37)) goLeft();
    else if((PREV) && (key == 39)) goRight();
    else if(key == 112)
    {
        if(event.preventDefault) event.preventDefault()
        else event.returnValue = false;
        window.open("./readme.html");
    }
}

window.onhashchange = hashchange;
var HASH = window.location.hash;
function hashchange()
{
    var i = window.location.hash.search("photonum");
    if(PREV && (i == -1)) closePreview();
    else if((PREV) && (i != -1)) changePreview();
    else if (i != -1) loadPreview();
}

function activePREVmode()
{
    var k = parseInt(window.location.hash.split("=")[1]);
    setCookie("photonum", k+"",
            {
                expires: 9999999999,
                path: "/photo/",
                domain: "anotherone.esy.es"
            }
    );
}

function deactivePREVmode()
{
    setCookie("photonum", "",
            {
                expires: -1,
                path: "/photo/",
                domain: "anotherone.esy.es"
            }
    );
}