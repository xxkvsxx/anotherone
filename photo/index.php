<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "../css/menu.css";
        @import "../css/photo.css";
    </style>
    <link rel="shortcut icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="../index.js"></script>
    <script type="text/javascript" src="./index.js"></script>
</head>
<body onresize="res(true)" onkeyup="keyAction(event)" onkeydown="keyD(event)">
    <?php
        require("../php/script.php");
        menu();
        upPointer();
    ?>
    <div class="BUFFER cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 main">
        <h1 class="h1">Фотогалерея</h1>
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 galery">
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>
    
    <div class="BUFFER cell12"></div>
    <script>loadmin()</script>
</body>
</html>