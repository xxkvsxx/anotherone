
/* Аналог getElementsByClassName с расширением для IE8*/
function getClassElements(obj, st)
{
    var A = [];
    try
    {
        var B = obj.getElementsByClassName(st);
        for(var i=0; B[i]; ++i) A.push(B[i]);

    }
    catch(e)
    {
        elements = obj.getElementsByTagName("*");
        for(var i=0; i<elements.length; ++i)
        {
            cl = elements[i].className;
            if(!cl) continue;
            try
            {
                cl = cl.split(" ");
            }
            catch(e)
            {
                continue;
            }
            for(var j=0; j<cl.length; ++j)
            {
                if(cl[j] == st) A.push(elements[i]);
            }
        }
    }
    return A;
}

/* Функция показывает время и дату и добавляет картинку месяца */
var month = 12;
var arrmonth = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
function time()
{
    rt = "/css/images/month/";
    var nw = new Date();
    if(month != nw.getMonth())
    {
        month = nw.getMonth();
        document.getElementById("menu-img").setAttribute("alt", arrmonth[month]);
        document.getElementById("menu-img").setAttribute("title", arrmonth[month]);
        document.getElementById("menu-img").setAttribute("src", rt+month+".png");
    }
    document.getElementById("DT").innerHTML = "";
    document.getElementById("DT").innerHTML = nw.toLocaleString();
}

/* Для мобильной версии, смена картинки */
function menupress(event, obj)
{
    var im = obj.getElementsByTagName("img")[0];
    var src = im.getAttribute("src");
    im.setAttribute("src", src.substring(0, src.length-4)+"-hover.png");
}

function unmenupress(event, obj)
{
    var im = obj.getElementsByTagName("img")[0];
    var src = im.getAttribute("src");
    im.setAttribute("src", src.substring(0, src.length-10)+".png");
}

/* Изменение шрифта в зависимости от разрешения */
var height;
var width;
var IH = 955;
var IW = 1920;
var mq;
function res(photo)
{
    width = window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth;
    height = window.innerHeight ||
            document.documentElement.clientHeight ||
            document.body.clientHeight;
    try { mq = window.matchMedia("(orientation: portrait)").matches; }
    catch(e) { mq = false; };
    var bd = document.getElementsByTagName("body")[0];
    bd.style.fontSize = width/IW*1.0+"em";
    if (photo) photores();
}

function getCookie(name)
{
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options)
{
    if (!navigator.cookieEnabled)
    {
        alert( 'Включите cookie для активации автоматического режима Preview' );
    }
    options = options || {};
    var expires = options.expires;
    if (typeof expires == "number" && expires) 
    {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString)
    {
        options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options)
    {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true)
        {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function sendOff()
{
    var img = document.getElementById("P");
    img.src = "/php/PointerImage.php?off="+(-(new Date().getTimezoneOffset()));
}