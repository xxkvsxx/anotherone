<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "../css/menu.css";
        @import "../css/about.css";
        @import "../css/home.css";
    </style>
    <link rel="shortcut icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="../index.js"></script>
</head>
<body onresize="res(false)">
    <?php
        require("../php/script.php");
        menu();
        upPointer();
    ?>
    <div class="BUFFER cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 main">
        <h1 class="h1">Обо Мне...</h1>
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>

    <div class="cell2"></div>
    <div class="cell10">
        <div class="cell1"></div>
        <div class="cell10">
            <div class="mcell12 cell6">
                <img src="me.jpg" alt="Здесь должен был быть Я" title="KVSPhoto">
            </div>
            <div class="mcell12 cell6">
                <ul>
                    <li><p>Это Я, Кулаков Владислав Сергеевич. &laquo;KVS&raquo;</p></li>
                    <li>
                        <p>Студент 3-его курса </p>
                        <a href="http://imkn.urfu.ru/" target="_blank">ИМКН,</a>
                        <br>
                        <a href="http://urfu.ru/" target="_blank">Уральского федерального университета</a>
                    </li>
                    <li>
                        <p>Эта мой первый продуманный сайт, этакая &quot;проба пера&quot;, так что он не слишком красив и написан примитивным языком (без фреймворков)</p>
                    </li>
                    <li>
                        <p> Можно присылать критику и мысли по улучшению<p>
                    </li>
                    <li>
                        <p>Email: </p><a href="mailto:18kvs02@gmail.com">18kvs02@gmail.com</a>
                    </li>
                    <li>
                        <p>И ещё несколько ссылок внизу &#9786;<p>
                    </li>
                </ul>
            </div>
            <div class="cell12"></div>
            <div id="KVS" class="cell3">
                <img src="kvs.png" alt="KVSLogo" title="KVSLogo">
            </div>
            <a href="https://vk.com/xxkvsxx" target="_blank" class="cell3 block" onmousedown="menupress(event, this)" onmouseup="unmenupress(event, this)">
                <img src="vk.png" alt="VK" title="VK">
            </a>
            <a href="https://twitter.com/xxKVSxx" target="_blank" class="cell3 block" onmousedown="menupress(event, this)" onmouseup="unmenupress(event, this)">
                <img src="tw.png" alt="Twitter" title="Twitter">
            </a>
            <a href="https://bitbucket.org/xxkvsxx/" target="_blank" class="cell3 block" onmousedown="menupress(event, this)" onmouseup="unmenupress(event, this)">
                <img src="bb.png" alt="Bitbucket" title="Bitbucket">
            </a>
        </div>
        <div class="cell1"></div>
    </div>
    <div class="BUFFER cell12"></div>
</body>
</html>