<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "../css/menu.css";
        @import "../css/home.css";
        .quests > h1
        {
            text-align: center;
            margin: 0%;
            font-family: 'Monotype-Corsiva-Regular';
            font-size: 300%;
        }
        .quest
        {
            background-color: black;
        }
        .orng
        {
            background-color: rgb(255, 140, 0);
        }
        .orng > p, .tr > p, .ans > p
        {
            margin: 0;
            font-family: 'HelveticaRegular';
            font-size: 120%;
        }
        .quest > h2
        {
            font-family: 'HelveticaRegular';
            font-size: 170%;
        }
        .quest > label
        {
            font-family: 'HelveticaRegular';
            font-size: 150%;
        }
        @media all and (orientation: portrait){
            .quests > h1
            {
                margin-top: 1%;
                font-size: 700%;
            }
            .quest > h2
            {
                font-size: 450%;
            }
            .quest > label
            {
                font-size: 400%;
            }
            .orng > p, .tr > p, .ans > p
            {
                font-size: 300%;
            }
        }
    </style>
    <link rel="shortcut icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="../index.js"></script>
    <script type="text/javascript" src="./index.js"></script>
</head>
<body onresize="res(false)">
    <?php
        require("../php/script.php");
        menu();
        upPointer();
    ?>
    <div class="BUFFER cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 main">
        <h1 class="h1">Статистика Сайта</h1>
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>
    <div class="cell2"></div>
    <div class="cell10">
        <div class="mcell12 cell6">
            <div class="cell1"></div>
            <img id="P" class="cell10" src="#" alt="Счётчик" title="Счётчик">
            <script>
                sendOff();
            </script>
            <div class="cell1"></div>
            <div class="cell12"></div>
        </div>
        <div class="mcell12 cell6">
            <div class="cell1"></div>
            <div class="cell10 quests">
                <h1>Маленький Опрос</h1>
            </div>
            <div class="cell1"></div>
        </div>
    </div>
    <div class="BUFFER cell12"></div>
    <script>addQuests();</script>
</body>
</html>