Quests = [
    {
        mess: "Как вы оцениваете мой сайт?",
        answers: ["Он прекрасен! :-)", "Прикольный.", "Ну так...", "Bad, very Bad. :-("]
    },
    {
        mess: "Откуда вы?",
        answers: ["Россия, РФ", "Другая Известная Страна", "Другая Малоизвестная Страна", "Из Другого Мира :-)"]
    },
    {
        mess: "Считаете ли вы себя &quot;Ещё Одним&quot; (Another One)? (Посмотрим сколько нас)",
        answers: ["Да, Всегда!", "Да, Иногда!", "Нет, Никогда!"]
    }
]

function addQuests()
{
    pr = getClassElements(document, "quests")[0];
    for(var i=0; i<Quests.length; ++i) createQuest(pr, i, Quests[i].mess, Quests[i].answers);
    askServer();
    setInterval(askServer, 2500);
}

function askServer()
{
    try
    {
        x = new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
        x.open('POST', "./redata.php", true);
        x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        x.onreadystatechange = function ()
        {
            if(x.readyState > 3) checkAnswers(x.responseText);
        };
        x.send();
    }catch (e)
    {
        return;
    }
}

function createQuest(pr, qid, mess, answers)
{
    var bd = document.createElement("div");
    bd.setAttribute("id", qid);
    bd.setAttribute("class", "quest cell12");
    var qbl = document.createElement("h2");
    qbl.innerHTML = (qid+1)+". "+mess;
    bd.appendChild(qbl);
    for(var i=0; i<answers.length; ++i)
    {
        var tx = document.createElement("label");
        var rd = document.createElement("input");
        var br = document.createElement("br");
        rd.setAttribute("type", "radio");
        rd.setAttribute("name", qid);
        rd.setAttribute("value", i);
        rd.setAttribute("onclick", "sendAnswer(this)")
        tx.appendChild(rd);
        tx.innerHTML += " "+answers[i];
        bd.appendChild(tx);
        bd.appendChild(br);
    }
    pr.appendChild(bd);
}

function sendAnswer(obj)
{
    qid = obj.getAttribute("name");
    ans = obj.getAttribute("value");
    try
    {
        x = new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
        x.open('POST', "./redata.php", true);
        x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        x.onreadystatechange = function ()
        {
            if(x.readyState > 3) checkAnswers(x.responseText);
        };
        x.send("qid="+qid+"&ans="+ans);
    } catch (e) {
        window.console && console.log(e);
    }
}

function checkAnswers(st)
{
    if(!st) return;
    try
    {
        var QA = [];
        for(var i=0; i<Quests.length; ++i)
        {
            QA.push([]);
            for(var j=0; j<Quests[i].answers.length; ++j) QA[i].push(0);
        }
        var ActiveQ = [];
        st = st.split("&");
        for(var i=0; i<st.length; ++i)
        {
            var p = st[i].split("/");
            QA[Number(p[0])][Number(p[1])] = Number(p[2]);
            ActiveQ.push(Number(p[0]));
        }
    }
    catch (e)
    {
        return;
    }
    reBuild(QA, ActiveQ);
}

function reBuild(QA, ActiveQ)
{
    for(var i=0; i<ActiveQ.length; ++i)
    {
        var Q = ActiveQ[i];
        bd = document.getElementById(""+Q);
        if(bd.getElementsByTagName("input").length) createAnswer(bd);
        var PR = [];
        var t = 0;
        for(var j=0; j<QA[Q].length; ++j) t+=QA[Q][j];
        for(var j=0; j<QA[Q].length; ++j) PR.push(QA[Q][j]/t*100);
        for(var j=0; j<QA[Q].length; ++j)
        {
            var pr = document.getElementById(Q+"perp"+j);
            pr.style.width = PR[j]+"%";
            var trp = document.getElementById(Q+"trp"+j);
            trp.innerHTML = Math.floor(PR[j])+"% / "+QA[Q][j];
        }
    }
}

function createAnswer(bd)
{
    qid = Number(bd.getAttribute("id"));
    var lb = bd.getElementsByTagName("label");
    while(lb.length) bd.removeChild(lb[0]);
    var br = bd.getElementsByTagName("br");
    while(br.length) bd.removeChild(br[0]);
    for(var i=0; i<Quests[qid].answers.length; ++i)
    {
        var ans = document.createElement("div");
        ans.setAttribute("class", "cell4 ans");
        ans.innerHTML = "<p>"+Quests[qid].answers[i]+"</p>"
        var pr = document.createElement("div");
        pr.setAttribute("class", "cell6 per");
        pr.setAttribute("id", qid+"per"+i);
        var p = document.createElement("div");
        p.setAttribute("id", qid+"perp"+i);
        p.setAttribute("class", "orng");
        p.innerHTML = "<p>&nbsp;</p>";
        pr.appendChild(p);
        var tr = document.createElement("div");
        tr.setAttribute("class", "cell2 tr");
        tr.setAttribute("id", qid+"tr"+i);
        var t = document.createElement("p");
        t.setAttribute("id", qid+"trp"+i);
        tr.appendChild(t);
        bd.appendChild(ans);
        bd.appendChild(pr);
        bd.appendChild(tr);
        var buf = document.createElement("div");
        buf.setAttribute("class", "cell12");
        bd.appendChild(buf);
    }
}