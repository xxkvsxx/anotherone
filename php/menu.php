<?php
    function menu()
    {
        echo ("<script>res(false)</script>
    <div class=\"LEFT\"></div>
    <div class=\"UP\"></div>
    <div class=\"DOWN\">
        <p>© В. С. Кулаков, 2015</p>
    </div>
    <div class=\"MENU-UP\">
        <p>MENU</p>
        <hr>
        <div class=\"MENU\">
            <a href=\"/\">Главная</a>
            <a href=\"/some/\">Избранное</a>
            <a href=\"/photo/\">Галерея</a>
            <a href=\"/stat/\">Статистика</a>
            <a href=\"/about/\">Обо Мне</a>
        </div>
        <div class=\"M-MENU\">
            <a href=\"/\" onmousedown=\"menupress(event, this)\">
                <img src=\"/css/images/home.png\" alt=\"Главная\">
            </a>
            <a href=\"/some/\" onmousedown=\"menupress(event, this)\">
                <img src=\"/css/images/some.png\" alt=\"Избранное\">
            </a>
            <a href=\"/photo/\" onmousedown=\"menupress(event, this)\">
                <img src=\"/css/images/photo.png\" alt=\"Галерея\">
            </a>
            <a href=\"/stat/\" onmousedown=\"menupress(event, this)\">
                <img src=\"/css/images/stat.png\" alt=\"Статистика\">
            </a>
            <a href=\"/about/\" onmousedown=\"menupress(event, this)\">
                <img src=\"/css/images/about.png\" alt=\"Обо Мне\">
            </a>
        </div>
    </div>
    <div class=\"MENU-DOWN\">
        <img id=\"menu-img\" alt=\"Месец\" src=\"/css/images/kvs.png\">
        <p id=\"DT\"></p>
        <script>time(); setInterval(time, 500);</script>
    </div>");
    }
?>