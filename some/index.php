<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "../css/menu.css";
        @import "../css/home.css";
    </style>
    <link rel="shortcut icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="../css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="../index.js"></script>
</head>
<body onresize="res(false)">
    <?php
        require("../php/script.php");
        menu();
        upPointer();
    ?>
    <div class="BUFFER cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 main">
        <h1 class="h1">Избранное</h1>
    </div>
    <div class="cell1"></div>
    <div class="cell12"></div>

    <div class="cell2"></div>
    <div class="cell10">
        <div class="h2">
            <p> Домашние работы по WEB и DHTML </p>
        </div>
        <hr>
        <ul>
            <li><a href="./hw/01Timetable/">Домашняя работа 01 Расписание</a></li>
            <li><a href="./hw/02Registration/">Домашняя работа 02 Регистрация</a></li>
            <li><a href="./hw/03BadCSS/"> Домашняя работа 03 CSS Print</a></li>
            <li><a href="./hw/04Footer/">Домашняя работа 04 Footer</a></li>
            <li><a href="./hw/05Block/">Домашняя работа 05 Летающий Блок</a></li>
            <li><a href="./hw/06Magic/">Домашняя работа 06 Магия</a></li>
        </ul>
        
    </div>
    <div class="BUFFER cell12"></div>
</body>
</html>