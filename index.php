<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Another One</title>
    <style type="text/css" media="all">
        @import "./css/menu.css";
        @import "./css/home.css";
    </style>
    <link rel="shortcut icon" href="./css/images/anotherone.ico" type="image/x-icon">
    <link rel="icon" href="./css/images/anotherone.ico" type="image/x-icon">
    <script type="text/javascript" src="./index.js"></script>
</head>
<body onresize="res(false)">
    <?php
        require("./php/script.php");
        menu();
        upPointer();
    ?>
    <div class="BUFFER cell12"></div>

    <div class="cell2"></div>
    <div class="cell1"></div>
    <div class="cell8 main">
        <h1 class="h1">Another One</h1>
        <img src="./css/images/anotherone.png" alt="An.ONE">
    </div>
    <div class="cell1"></div>

    <div class="cell2"></div>
    <div class="cell10">
        <p class="h2"> Вас приветсвует Сайт AnotherOne!</p>
        <div class="cell1"></div>
        <div class="cell10">
            <ul>
                <li>
                    <p>Сайт очень прост и понятен.</p>
                </li>
                <li>
                    <p>&laquo;Главная&raquo; страница у вас сейчас на Экране.</p>
                </li>
                <li>
                    <p>На странице &laquo;Избранное&raquo; вы найдёте несколько ссылок, в том числе на мои работы.</p>
                </li>
                <li>
                    <p>В &laquo;Галерее&raquo; вы найдёте несколько интересных фотографий и картинок (Не забудьте там нажать &laquo;F1&raquo; и почитать справку &#9786;)</p>
                </li>
                <li>
                    <p>Страница &laquo;Статистика&raquo; покажет информацию о ваших визитах. Также вы сможете пройти небольшой опрос.</p>
                </li>
                <li>
                    <p>На странице &laquo;Обо мне&raquo; можете прочесть информацию о создателе этого сайта и несколько ссылок, по которым можно со мной связаться</p>
                </li>
                <li>
                    <p>А может потом добавлю ещё разделов, так что пока &laquo;&quot;Оставлю это здесь&quot; Автор&raquo;.</p>
                </li>
            </ul>
        </div>
        <div class="cell1"></div>
    </div>
    <div class="BUFFER cell12"></div>
</body>
</html> 